# -*- coding: utf-8 -*-
from typing import Iterable

from terms.ascii_doc_table_terms import AsciiDocTableTerms
from terms.const import _temp_terms_basic, read_lines_file, write_file, read_file, _TEMPORARY_TERMS

_MISSING_: str = '\033[41m'
_FOUND_: str = '\033[42m'
_NORMAL_: str = '\033[0m'

_conversion: dict[bool, str] = {
    True: " ",
    False: " not "
}


def update_table_terms():
    _path: str = "../../glossary/terms.adoc"
    content: list[str] = read_lines_file(_path)
    write_file(_temp_terms_basic, content)


def get_table_terms():
    lines: list[str] = read_file(_temp_terms_basic).split("\n")[6:-1]

    ascii_doc_table: AsciiDocTableTerms = AsciiDocTableTerms(lines)
    ascii_doc_table.complete()
    ascii_doc_table.set_terms()
    ascii_doc_table.set_dict_positions()

    return ascii_doc_table


def check_contains(value: str):
    return value.upper() in ascii_doc_table_terms


def found_value(value: str):
    result: list[str] = [f"{_FOUND_}{value} найден"]

    for term in ascii_doc_table_terms.get(value):
        result.append(f"{_FOUND_}{term.full}")

    result.append("-" * 50)
    return "\n".join(result)


def missing_value(value: str):
    result: list[str] = [f"{_MISSING_}{value} не найден", "-" * 50]
    return "\n".join(result)


def check_listed(values: str | Iterable[str] = None):
    if values is None:
        return "No values"

    elif isinstance(values, str):
        if check_contains(values):
            return found_value(values)

        else:
            return missing_value(values)

    else:
        result: list[str] = []
        _found: list[str] = []
        _missing: list[str] = []

        for value in values:
            if check_contains(value):
                _found.append(found_value(value))

            else:
                _missing.append(missing_value(value))

        result.append("Найдены:")
        result.extend(_found)
        result.append(_NORMAL_)
        result.append("Не найдены:")
        result.extend(_missing)
        result.append(_NORMAL_)

        return "\n".join(result)


def check_duplicates():
    _repeats: list[str] = ["Дублирующиеся термины:"]

    for short, terms in ascii_doc_table_terms.dict_terms.items():
        if len(terms) > 1:
            _full: list[str] = [term.full for term in terms]

            if len(_full) != len(set(_full)):
                _repeats.append(short)

    if len(_repeats) == 1:
        return "Дублирующихся терминов не найдено"

    else:
        return "\n".join(_repeats)


def get_position(value: str):
    return ascii_doc_table_terms.get_positions(value)


def get_temp_dir():
    return _TEMPORARY_TERMS


if __name__ == '__main__':
    ascii_doc_table_terms: AsciiDocTableTerms = get_table_terms()
    values: list[str] = [
        "ABM",
        "ACDC",
        "ADCCP",
        "ADM",
        "ADSL",
        "AFC",
        "ALCAP",
        "AM",
        "ANR",
        "ARD",
        "ARM",
        "ASME",
        "ATSSS",
        "AWoPDN",
        "BCCH",
        "BNG",
        "BTS",
        "CAT",
        "CoMP",
        "CPRI",
        "CRL",
        "DDDS",
        "DESS",
        "DSAP",
        "DSLAM",
        "EDT",
        "EEA",
        "EENLV",
        "EIA",
        "FDD",
        "FTM",
        "GEA",
        "GLI",
        "GMSK",
        "HRNN",
        "IAB",
        "IHCSI",
        "IMP",
        "IPUPS",
        "IRC",
        "ISR",
        "LAP",
        "MICO",
        "MSAS",
        "NACC",
        "NCC",
        "NCGI",
        "NEAF",
        "NG-FW",
        "NH",
        "NIC",
        "NMG",
        "NMO",
        "NRM",
        "NSI",
        "OBW",
        "OCSP",
        "OSF",
        "OSP",
        "PAA",
        "PEI",
        "PRACH",
        "PRINS",
        "PSK",
        "PSN",
        "PTI",
        "RACS",
        "RFSP-ID",
        "RGW",
        "SCNIPDN",
        "SDLC",
        "SEPP",
        "SGNIPDN",
        "SNA",
        "SNPN",
        "SRB",
        "SSAP",
        "SSD",
        "SUCI",
        "SUPI",
        "TDD",
        "TLD",
        "TLIV",
        "TSC",
        "UAS",
        "UAV",
        "UEA",
        "UIA",
        "URA",
        "URSP",
        "V2X",
        "VAMOS",
        "VDI",
        "WOC",
        "WUS",
        "WWSF",
        "XID",
    ]
    print(check_listed(values))
