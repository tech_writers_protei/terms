#!/bin/bash

mkdir "exe_files"

if [[ "${OSTYPE}" == "msys"* ]]; then
	pyinstaller --noconfirm --distpath "./exe_files" "terms.exe.spec"
elif [[ "${OSTYPE}" == "linux"* || "${OSTYPE}" == "darwin"* ]]; then
	pyinstaller --noconfirm --distpath "./exe_files" "terms.spec"
fi

ls -a "exe_files"
rm -rf "exe_files"
