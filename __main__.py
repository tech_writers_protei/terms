# -*- coding: utf-8 -*-
import os.path as p
from importlib import import_module
from os import makedirs
from subprocess import CalledProcessError, run

# noinspection PyProtectedMember
from terms.const import _log_folder, _TEMPORARY_TERMS
from terms.custom_exceptions import ImportLibraryDependencyFailedError

if __name__ == '__main__':
    _packages: tuple[str, ...] = ("loguru",)

    for _package in _packages:
        try:
            import_module(_package)

        except (ModuleNotFoundError, ImportError):
            print(f"Загрузка пакета {_package} ...")

            try:
                run(["python", "-m", "pip", "install", _package]).returncode

            except CalledProcessError as e:
                print(f"{e.__class__.__name__}\nКод ответа {e.returncode}\nОшибка {e.output}")
                print(f"Не удалось импортировать пакет `{_package}`.")
                raise ImportLibraryDependencyFailedError

            except OSError as e:
                print(f"{e.__class__.__name__}\nФайл {e.filename}\nОшибка {e.strerror}")
                print(f"Не удалось импортировать пакет `{_package}`")
                raise ImportLibraryDependencyFailedError

            else:
                print(f"Пакет {_package} установлен")

        except (OSError, RuntimeError, MemoryError) as e:
            print(f"Произошла внутренняя ошибка.")
            print(f"{e.__class__.__name__}, {str(e)}")
            raise

    if not p.exists(_log_folder):
        try:
            makedirs(_log_folder, exist_ok=True)

        except (RuntimeError, PermissionError, OSError) as e:
            print(f"{e.__class__.__name__}\n{str(e)}")
            raise

    _TEMPORARY_TERMS.mkdir(parents=True, exist_ok=True)

    from terms.main import run_script

    run_script()
