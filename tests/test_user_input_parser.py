# -*- coding: utf-8 -*-

from tests.conftest import interceptor, user_input_parser
from terms.user_input import State, UserInputParser


def test_line_parser(user_input_parser: UserInputParser):
    user_input_parser._input = "test_line"
    assert user_input_parser.line_parser()


def test_handle_default_input(user_input_parser: UserInputParser):
    user_input_parser._input = None
    user_input_parser.handle_input()
    assert user_input_parser._input is None


def test_handle_default_input_intercepted(user_input_parser: UserInputParser, interceptor: str):
    user_input_parser.handle_input(interceptor)
    assert user_input_parser.state == State.STOPPED
