# -*- coding: utf-8 -*-
from pytest import raises

from tests.conftest import version, string_version, null_version, string_null_version, version_line, empty_line, invalid_line
from terms.custom_exceptions import InvalidVersionError
from terms.version_container import Version


def test_equal_version(version: Version, string_version: Version):
    assert version == string_version


def test_null_version(null_version: Version, string_null_version: Version):
    assert null_version != string_null_version


def test_unequal_version(version: Version, null_version: Version):
    assert version != null_version


def test_from_string(version_line: str):
    assert Version.from_string(version_line) == Version("01", "01", "2000")


def test_from_empty_string(empty_line: str, null_version: Version):
    assert str(Version.from_string(empty_line)) == str(null_version)


def test_from_invalid_line(invalid_line: str):
    with raises(InvalidVersionError):
        print("\n")
        Version.from_string(invalid_line)
