# -*- coding: utf-8 -*-
import random
import string
from pathlib import Path
from typing import Mapping, Sequence

import pytest

from terms.ascii_doc_table_terms import AsciiDocTableTerms
# noinspection PyProtectedMember
from terms.const import StrPath, _temp_version, _temp_terms
from terms.content_git_page import ContentGitPage
# noinspection PyProtectedMember
from terms.table import _Term
# noinspection PyProtectedMember
from terms.user_input import LineParser, UserInputParser, _interceptors
from terms.version_container import VersionContainer, Version


class Char(str):
    def __new__(cls, value):
        if len(str(value)) != 1:
            raise ValueError("Char must only a single character.")
        char = super().__new__(cls, value)
        return char

    def __init__(self, value):
        if isinstance(value, str):
            self.value = value
        else:
            self.value = str(value)

    def __str__(self):
        return f"{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


def multiple_insert(base: Sequence[str], items: Mapping[int, Char] = None):
    _max: int = max(items.keys())

    if _max > len(base):
        raise ValueError(f"Maximum index {_max} exceeds the base line length {len(base)}")

    if items is None:
        items: dict[int, Char] = dict()

    else:
        items: dict[int, Char] = {k: Char(items.get(k)) for k in sorted(items, reverse=True)}

    _base: list[str] = list(base)

    for k, v in items.items():
        _base.insert(k, v.value)

    return "".join(_base)


def generate_line_split() -> str:
    _available_chars: str = f"{string.ascii_letters}{string.digits}"
    _chars: tuple[Char, ...] = (Char("-"), Char("_"))
    _punctuation: str = " ,.:;/"
    _k_required: int = 3
    _required: list[str] = random.choices(_punctuation, k=_k_required)
    _k_any: int = random.randint(25, 27) - _k_required
    _any: list[str] = random.choices(_available_chars, k=_k_any)
    items: dict[int, Char] = {
        5: Char(_required[0]),
        11: Char(_required[1]),
        19: Char(_required[2])
    }
    return multiple_insert(_any, items)


def generate_line_start(start: Sequence[str]) -> str:
    _line: str = generate_line_split()
    _index: int = random.randint(0, len(start) - 1)
    _start: str = start[_index]
    _prefix: str = _line[:len(_start)]

    return f"{_start}{_line.removeprefix(_prefix)}"


lines_start: tuple[str, ...] = (
    "start", "begin", "line", "test", "inspect"
)

separators_marks: str = "['!$@<> .,;:*|\\/+#{}=]"


def split_line(line: str) -> tuple[str, ...]:
    return line[:5], line[6:12], line[13:21], line[22:]


def generate_line_multisplit() -> str:
    _line: str = generate_line_split()
    _join_char_index: int = random.randint(0, len(separators_marks) - 1)
    _join_char: str = separators_marks[_join_char_index]
    return _join_char.join(split_line(_line))


@pytest.fixture(scope="module")
def line():
    return generate_line_split()


@pytest.fixture(scope="module", params=[generate_line_split()])
def line_parser(line):
    return LineParser(line)


@pytest.fixture(scope="module", params=[generate_line_start(lines_start)])
def start_line_parser(line):
    return LineParser(line)


@pytest.fixture(scope="module", params=[generate_line_multisplit()])
def multi_line_parser(line):
    return LineParser(line)


@pytest.fixture(scope="module")
def ascii_doc_table_terms():
    print(Path.cwd())
    print(list(Path.cwd().iterdir()))
    with open("sources/ascii_doc_table_terms.adoc") as f:
        lines: list[str] = f.readlines()
    return AsciiDocTableTerms(lines)


@pytest.fixture(scope="module")
def user_input_parser(ascii_doc_table_terms):
    return UserInputParser(ascii_doc_table_terms)


@pytest.fixture(scope="module")
def interceptor():
    _index: int = random.randint(0, len(_interceptors) - 1)
    return _interceptors[_index]


@pytest.fixture(scope="module")
def term():
    short: str = "LTA"
    full: str = "List of Terms and Abbreviations"
    rus: str = "список терминов и сокращений"
    commentary: str = "раздел документа, содержащий все используемые термины и аббревиатуры"
    return _Term(short, full, rus, commentary)


@pytest.fixture(scope="module")
def null_term():
    return _Term()


@pytest.fixture(scope="module")
def version_validator():
    return VersionContainer()


@pytest.fixture(scope="module")
def version():
    return Version(29, 4, 2014)


@pytest.fixture(scope="module")
def null_version():
    return Version(0, 0, 0)


@pytest.fixture(scope="module")
def string_version():
    return Version("29", "4", "2014")


@pytest.fixture(scope="module")
def string_null_version():
    return Version("0", "0", "0")


def read(file: StrPath):
    with open(file, "r") as f:
        _: list[str] = f.readlines()

    return _


@pytest.fixture(scope="module")
def content_git_version_file():
    content_git_version: ContentGitPage = ContentGitPage(57022544, "__version__.txt", _temp_version)
    content_git_version.content = read(Path("sources/__version__.txt"))
    return content_git_version


@pytest.fixture(scope="module")
def content_git_terms_file():
    content_git_terms = ContentGitPage(57022544, "terms.adoc", _temp_terms)
    content_git_terms.content = read(Path("sources/terms.adoc"))
    return content_git_terms


@pytest.fixture(scope="module")
def version_line():
    return "01.01.2000"


@pytest.fixture(scope="module")
def empty_line():
    return str()


@pytest.fixture(scope="module")
def invalid_line():
    return "29042000"
