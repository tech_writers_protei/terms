# -*- coding: utf-8 -*-
from tests.conftest import null_term, term


def test_formatting_type(term):
    assert term._formatting_type() == 7


def test_bool(term):
    assert bool(term) is False


def test_bool_null(null_term):
    assert bool(null_term) is True


def test_formatting_description(term):
    assert term._formatting_description() == f"{term.full}, {term.rus} — {term.commentary}"


def test_formatting_description_null(null_term):
    assert null_term._formatting_description() == ""


def test_abbr(term):
    assert term.abbr() == f'<abbr title="{term.full}">{term.short}</abbr>'


def test_abbr_null(null_term):
    assert null_term.abbr() == f"Не найдено"


def test_formatted(term):
    assert term.formatted() == f"{term.short}\n{term._formatting_description()}"


def test_formatted_null(null_term):
    assert null_term.formatted() == f"Не найдено"
