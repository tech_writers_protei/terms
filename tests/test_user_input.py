# -*- coding: utf-8 -*-

from tests.conftest import lines_start, separators_marks, split_line, line_parser, start_line_parser, line, multi_line_parser
from terms.user_input import LineParser


def test_up(line_parser: LineParser):
    assert line_parser.up == line_parser.line.upper()


def test_down(line_parser: LineParser):
    assert line_parser.down == line_parser.line.lower()


def test_parse_user_input_default_line(line_parser: LineParser):
    assert line_parser.parse_user_input() == list(map(lambda x: x.upper(), split_line(line_parser.line)))


def test_parse_user_input(line: str):
    _line_parser: LineParser = LineParser("line_parser")
    assert _line_parser.parse_user_input(line) == list(split_line(LineParser(line).line))


def test_check_line_start_defaults(start_line_parser: LineParser):
    assert start_line_parser.check_line_start() is False


def test_check_line_start_default_line(start_line_parser: LineParser):
    assert start_line_parser.check_line_start(start=lines_start) is False


def test_check_line_start_default_start(line: str):
    assert LineParser("line_parser").check_line_start(line) is False


def test_multisplit_defaults(line_parser: LineParser):
    assert line_parser.multi_split() == list(map(lambda x: x.lower(), split_line(line_parser.line)))


def test_multisplit_default_separators(line: str):
    assert LineParser("line_parser").multi_split(line) == [*split_line(line)]


def test_multisplit_default_line(multi_line_parser: LineParser):
    assert multi_line_parser.multi_split(separators=separators_marks) == list(
        map(lambda x: x.lower(), split_line(multi_line_parser.line)))
